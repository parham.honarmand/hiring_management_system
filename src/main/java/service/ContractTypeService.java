package service;

import model.ContractType;
import repository.GeneralRepository;
import repository.JDBCImpl.ContractTypeDao;
import utility.ObjectNotFoundException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ContractTypeService implements Service<ContractType> {
    private ContractTypeDao contractTypeDao = GeneralRepository.getInstance().getContractTypeDao();

    public void insert(ContractType contractType) {
        try {
            contractTypeDao.insert(contractType);
        } catch (SQLException e) {
            System.out.println("can not insert this contract type : " + contractType.getType());
        }
    }

    public void update(ContractType contractType) {
        try {
            contractTypeDao.update(contractType);
        } catch (SQLException e) {
            System.out.println("can not insert this contract type : " + contractType.getType());
        }
    }

    public ContractType searchById(Long id) throws ObjectNotFoundException {
        try {
            return contractTypeDao.searchById(id);
        } catch (SQLException e) {
            throw new ObjectNotFoundException("this contract type is not found");
        }
    }

    public List<ContractType> findAll() {
        try {
            return contractTypeDao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
