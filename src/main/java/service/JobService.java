package service;

import model.Job;
import repository.GeneralRepository;
import repository.JDBCImpl.JobDao;
import utility.ObjectNotFoundException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JobService implements Service<Job> {
    private JobDao jobDao = GeneralRepository.getInstance().getJobDao();

    public void insert(Job job) {
        try {
            jobDao.insert(job);
        } catch (SQLException e) {
            System.out.println("can not insert this job : " + job.getTitle());
        }
    }

    public void update(Job job) {
        try {
            jobDao.update(job);
        } catch (SQLException e) {
            System.out.println("can not update this job : " + job.getTitle());
        }
    }

    public Job searchById(Long id) throws ObjectNotFoundException {
        try {
            return jobDao.searchById(id);
        } catch (SQLException e) {
            throw new ObjectNotFoundException("this job does not found");
        }
    }

    public List<Job> findAll() {
        try {
            return jobDao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
