package service;

import model.Company;
import repository.GeneralRepository;
import repository.JDBCImpl.CompanyDao;
import utility.Encryptor;
import utility.ObjectNotFoundException;

import java.sql.SQLException;
import java.util.*;

public class CompanyService implements Service<Company> {
    private CompanyDao companyDao = GeneralRepository.getInstance().getCompanyDao();

    public void insert(Company company) {
        try {
            company.setPassword(Encryptor.getMd5(company.getPassword()));
            companyDao.insert(company);
        } catch (SQLException e) {
            System.out.println("can not insert this company :" + company.getName());
        }
    }

    public void update(Company company) {
        try {
            companyDao.update(company);
        } catch (SQLException e) {
            System.out.println("can not update this company :" + company.getName());
        }
    }

    public Company searchById(Long id) throws ObjectNotFoundException {
        try {
            return companyDao.searchById(id);
        } catch (Exception e) {
            throw new ObjectNotFoundException("this company not found");
        }
    }

    public List<Company> findAll() {
        try {
            return companyDao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public Company searchByUsername(String username) {
        try {
            return companyDao.searchByUsername(username);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ObjectNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Company login(String username, String password) {
        try {
            Company company = companyDao.searchByUsername(username);
            if (company.getPassword().equals(Encryptor.getMd5(password)))
                return company;
            else
                System.out.println("wrong password");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ObjectNotFoundException e) {
            System.out.println("this company does not exist");
        }
        return null;
    }
}
