package service;

import model.JobPosition;
import repository.GeneralRepository;
import repository.JDBCImpl.JobPositionDao;
import utility.ObjectNotFoundException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JobPositionService implements Service<JobPosition> {
    private JobPositionDao jobPositionDao = GeneralRepository.getInstance().getJobPositionDao();

    public void insert(JobPosition jobPosition) {
        try {
            jobPositionDao.insert(jobPosition);
        } catch (SQLException e) {
            System.out.println("can not insert this job position : " + jobPosition.getTitle());
        }
    }

    public void update(JobPosition jobPosition) {
        try {
            jobPositionDao.update(jobPosition);
        } catch (SQLException e) {
            System.out.println("can not update this job position : " + jobPosition.getTitle());
        }
    }

    public JobPosition searchById(Long id) throws ObjectNotFoundException {
        try {
            return jobPositionDao.searchById(id);
        } catch (SQLException e) {
            throw new ObjectNotFoundException("this job position does not exist");
        }
    }

    public List<JobPosition> findAll() {
        try {
            return jobPositionDao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<JobPosition> searchByCompanyId(Long id) {
        try {
            return jobPositionDao.searchByCompanyId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public void updateByViewCount(JobPosition jobPosition) {
        try {
            jobPositionDao.updateByViewCount(jobPosition);
        } catch (SQLException e) {
            System.out.println("can not update this job position");
        }
    }
}
