package service;

public class GeneralService {
    private static GeneralService generalService;
    private CompanyService companyService;
    private EmployeeService employeeService;
    private JobPositionService jobPositionService;
    private JobService jobService;
    private ContractTypeService contractTypeService;
    private ApplyService applyService;

    public static GeneralService getInstance(){
        if(generalService == null)
            generalService = new GeneralService();
        return generalService;
    }

    private GeneralService(){
        companyService = new CompanyService();
        contractTypeService = new ContractTypeService();
        employeeService = new EmployeeService();
        jobPositionService = new JobPositionService();
        jobService = new JobService();
        applyService = new ApplyService();
    }

    public CompanyService getCompanyService() {
        return companyService;
    }

    public EmployeeService getEmployeeService() {
        return employeeService;
    }

    public JobPositionService getJobPositionService() {
        return jobPositionService;
    }

    public JobService getJobService() {
        return jobService;
    }

    public ContractTypeService getContractTypeService() {
        return contractTypeService;
    }

    public ApplyService getApplyService() {
        return applyService;
    }
}
