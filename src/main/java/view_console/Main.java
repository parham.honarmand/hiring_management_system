package view_console;

import controller.GeneralController;
import model.ContractType;
import model.Job;
import utility.Utilities;

public class Main {
    private static Login login = new Login();
    private static Register register = new Register();

    public static void main(String[] args) {
        firstMenu();
    }

    public static void firstMenu() {
        System.out.println("welcome to hiring management system :");
        System.out.println("1 - register");
        System.out.println("2 - login");
        System.out.println("3 - add job");
        System.out.println("4 - add contract type");
        System.out.println("enter a number : ");

        int number = Utilities.getInstance().getIntFromUser();

        switch (number) {
            case 1:
                register.register();
                firstMenu();
                break;
            case 2:
                login.login();
                break;
            case 3:
                addJob();
                firstMenu();
                break;
            case 4:
                addContractType();
                firstMenu();
                break;
            default:
                System.out.println("invalid number");
                firstMenu();
        }
    }

    private static void addContractType() {
        System.out.println("Add contract type :");

        System.out.println("please enter contract type :");
        String type = Utilities.getInstance().getStringFromUser();

        ContractType contractType = new ContractType(type);

        GeneralController.getInstance().getContractTypeController().insert(contractType);
        System.out.println("your contract type is now added");
    }

    private static void addJob() {
        System.out.println("Add job :");

        System.out.println("please enter job title :");
        String title = Utilities.getInstance().getStringFromUser();

        Job job = new Job(title);

        GeneralController.getInstance().getJobController().insert(job);
        System.out.println("your job is now added");
    }
}
