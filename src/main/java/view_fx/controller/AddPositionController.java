package view_fx.controller;

import controller.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import model.*;

import java.io.*;
import java.net.URL;
import java.util.List;

public class AddPositionController {

    @FXML
    private AnchorPane root;

    @FXML
    private TextField title;

    @FXML
    private TextField description;

    public static JobPosition jobPosition = new JobPosition();

    @FXML
    void handleBack(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/company_panel.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void handleNext(MouseEvent event) throws IOException {
        jobPosition.setTitle(title.getText());
        jobPosition.setDescription(description.getText());
        jobPosition.setCompany_id(CompanyController.currentCompany.getId());
        List<Job> jobs = GeneralController.getInstance().getJobController().findAll();

        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/add_position_job.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        FlowPane flowPane = new FlowPane(30, 30);

        for (int i = 0; i < jobs.size(); i++) {
            Job job = jobs.get(i);
            Button button = new Button(String.format("job title : %s", job.getTitle()));

            //get Job title from user
            button.setOnMouseClicked(e -> {
                jobPosition.setJob_id(job.getId());
                try {
                    URL url2 = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                            "src/main/java/view_fx/fxml/add_position_job.fxml").toURI().toURL();
                    AnchorPane anchorPane2 = FXMLLoader.load(url2);
                    FlowPane flowPane2 = new FlowPane(30, 30);
                    List<ContractType> contractTypes = GeneralController.getInstance().getContractTypeController().findAll();

                    for (int j = 0; j < contractTypes.size(); j++) {
                        ContractType contractType = contractTypes.get(j);
                        Button button2 = new Button(String.format("contract type : %s", contractType.getType()));

                        //get contract type from user
                        button2.setOnMouseClicked(ev -> {
                            jobPosition.setContract_type_id(contractType.getId());
                            GeneralController.getInstance().getJobPositionController().insert(jobPosition);
                            try {
                                URL url3 = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                                        "src/main/java/view_fx/fxml/company_panel.fxml").toURI().toURL();
                                AnchorPane anchorPane3 = FXMLLoader.load(url3);
                                button2.getScene().setRoot(anchorPane3);
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        });
                        flowPane2.getChildren().add(button2);
                    }

                    anchorPane2.getChildren().add(flowPane2);
                    button.getScene().setRoot(anchorPane2);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });

            flowPane.getChildren().add(button);
        }

        anchorPane.getChildren().add(flowPane);
        this.root.getScene().setRoot(anchorPane);
    }
}
