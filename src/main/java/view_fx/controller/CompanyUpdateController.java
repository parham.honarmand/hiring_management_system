package view_fx.controller;

import controller.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import model.Company;
import utility.Encryptor;

import java.io.*;
import java.net.URL;

public class CompanyUpdateController {

    @FXML
    private AnchorPane root;

    @FXML
    private TextField name;

    @FXML
    private TextField username;

    @FXML
    private TextField password;

    @FXML
    private TextField company_location;

    @FXML
    private TextField field;

    @FXML
    private TextArea description;

    @FXML
    private TextField employee_count;

    private boolean isSet;

    @FXML
    void handleBack(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/company_panel.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void handleUpdate(MouseEvent event) throws IOException {
        Company current = CompanyController.currentCompany;

        String pass = (password.getText().equals("") ? current.getPassword() : Encryptor.getMd5(password.getText()));
        Company company = new Company(current.getId(), name.getText(), username.getText(),
                pass, company_location.getText(), field.getText(), description.getText(),
                Integer.parseInt(employee_count.getText()));

        GeneralController.getInstance().getCompanyController().update(company);
        CompanyController.currentCompany = company;
        isSet = false;
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/company_panel.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void setValue() {
        if (isSet)
            return;
        Company company = CompanyController.currentCompany;

        name.setText(company.getName());
        username.setText(company.getUsername());
        description.setText(company.getDescription());
        company_location.setText(company.getLocation());
        field.setText(company.getField());
        employee_count.setText(String.valueOf(company.getEmployee_count()));
        isSet = true;
    }

}
