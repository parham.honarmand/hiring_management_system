package view_fx.controller;

import javafx.fxml.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.*;
import java.net.URL;

public class WrongInputController {

    @FXML
    private AnchorPane root;

    @FXML
    void handelOK(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/login.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }
}
