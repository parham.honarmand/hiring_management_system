package view_fx.controller;

import controller.*;
import javafx.fxml.*;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import model.*;
import utility.ObjectNotFoundException;

import java.io.*;
import java.net.URL;
import java.util.List;

public class CompanyPanelController {
    @FXML
    private AnchorPane root;

    @FXML
    void handleBack(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/company_login.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    public void addPosition(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/add_position.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    public void showMyPositions(MouseEvent event) throws IOException, ObjectNotFoundException {
        List<JobPosition> jobPositions = GeneralController.getInstance().getJobPositionController()
                .searchByCompanyId(CompanyController.currentCompany.getId());

        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/show_position.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        FlowPane flowPane = new FlowPane(30, 5);

        for (int i = 0; i < jobPositions.size(); i++) {
            Label label = new Label(getJobPositionString(jobPositions.get(i)));
            flowPane.getChildren().add(label);
        }

        anchorPane.getChildren().add(flowPane);
        this.root.getScene().setRoot(anchorPane);
    }

    private String getJobPositionString(JobPosition jobPosition) throws ObjectNotFoundException {
        ContractType contractType = GeneralController.getInstance().getContractTypeController()
                .searchById(jobPosition.getContract_type_id());
        Company company = GeneralController.getInstance().getCompanyController()
                .searchById(jobPosition.getCompany_id());
        return String.format("title : %s, description : %s, view count : %s, company : %s, type : %s",
                jobPosition.getTitle(), jobPosition.getDescription(), jobPosition.getView_count(),
                company.getName(), contractType.getType());
    }

    public void updateProfile(MouseEvent event) throws IOException, InterruptedException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/company_update.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }
}
