package view_fx.controller;

import javafx.fxml.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.*;
import java.net.URL;

public class RegisterController {
    @FXML
    private AnchorPane root;

    @FXML
    public void handleBack(MouseEvent mouseEvent) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/main.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void handleCompany(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/src/main/" +
                "java/view_fx/fxml/company_register.fxml").toURI().toURL();
        AnchorPane anchorPane = (AnchorPane) FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void handleEmployee(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/employee_register.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }
}
