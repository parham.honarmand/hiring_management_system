package view_fx.controller;

import controller.GeneralController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import model.Company;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class CompanyRegisterController {

    @FXML
    private AnchorPane root;

    @FXML
    private TextField name;

    @FXML
    private TextField username;

    @FXML
    private TextField password;

    @FXML
    private TextField company_location;

    @FXML
    private TextField field;

    @FXML
    private TextArea description;

    @FXML
    private TextField employee_count;

    @FXML
    void handleBack(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/register.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void handleRegister(MouseEvent event) throws IOException {
        Company company = new Company(name.getText(), username.getText(), password.getText(), company_location.getText(),
                field.getText(), description.getText(), Integer.parseInt(employee_count.getText()));

        GeneralController.getInstance().getCompanyController().insert(company);

        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/login.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }
}
