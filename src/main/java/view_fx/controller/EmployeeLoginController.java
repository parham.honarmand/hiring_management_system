package view_fx.controller;

import controller.*;
import javafx.fxml.*;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import model.Employee;

import java.io.*;
import java.net.URL;

public class EmployeeLoginController {
    @FXML
    private AnchorPane root;

    @FXML
    private TextField username;

    @FXML
    private TextField password;

    @FXML
    void handleBack(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/login.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    void handleLogin(MouseEvent event) throws IOException {
        String username = this.username.getText();
        String password = this.password.getText();

        Employee employee = GeneralController.getInstance().getEmployeeController().login(username, password);

        if (employee == null) {

            URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                    "src/main/java/view_fx/fxml/wrong_input.fxml").toURI().toURL();
            AnchorPane anchorPane = FXMLLoader.load(url);
            this.root.getScene().setRoot(anchorPane);

        } else {
            URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                    "src/main/java/view_fx/fxml/employee_panel.fxml").toURI().toURL();
            AnchorPane anchorPane = FXMLLoader.load(url);
            EmployeeController.currentEmployee = employee;
            this.root.getScene().setRoot(anchorPane);
        }
    }
}