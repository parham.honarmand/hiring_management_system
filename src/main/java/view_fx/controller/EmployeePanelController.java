package view_fx.controller;

import controller.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import model.*;
import utility.ObjectNotFoundException;

import java.io.*;
import java.net.URL;
import java.util.List;

public class EmployeePanelController {

    @FXML
    private AnchorPane root;

    @FXML
    void handleBack(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/employee_login.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void handleApplyingAJob(MouseEvent event) throws IOException, ObjectNotFoundException {
        List<JobPosition> jobPositions = GeneralController.getInstance()
                .getEmployeeController().getJobPositionsForEmployee(EmployeeController.currentEmployee);

        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/job_for_apply.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        FlowPane flowPane = new FlowPane(30, 10);

        for (int i = 0; i < jobPositions.size(); i++) {
            JobPosition jobPosition = jobPositions.get(i);
            Button button = new Button(getJobPositionString(jobPosition));
            button.setOnMouseClicked(e -> {
                GeneralController.getInstance().getApplyController().insert(new Apply(EmployeeController.currentEmployee.getId(),
                        jobPosition.getId(), String.valueOf(System.currentTimeMillis())));
                try {
                    URL url2 = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                            "src/main/java/view_fx/fxml/employee_panel.fxml").toURI().toURL();
                    AnchorPane anchorPane2 = FXMLLoader.load(url2);
                    button.getScene().setRoot(anchorPane2);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
            flowPane.getChildren().add(button);
            jobPosition.setView_count(jobPosition.getView_count() + 1);
            GeneralController.getInstance().getJobPositionController().updateByViewCount(jobPosition);
        }

        anchorPane.getChildren().add(flowPane);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void showAppliedJob(MouseEvent event) throws IOException, ObjectNotFoundException {
        List<JobPosition> jobPositions = GeneralController.getInstance().getApplyController()
                .getAppliedPositions(EmployeeController.currentEmployee.getId());

        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/job_for_apply.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        FlowPane flowPane = new FlowPane(30, 10);

        for (int i = 0; i < jobPositions.size(); i++) {
            Label label = new Label(getJobPositionString(jobPositions.get(i)));
            flowPane.getChildren().add(label);
        }

        anchorPane.getChildren().add(flowPane);
        this.root.getScene().setRoot(anchorPane);
    }

    private String getJobPositionString(JobPosition jobPosition) throws ObjectNotFoundException {
        ContractType contractType = GeneralController.getInstance().getContractTypeController()
                .searchById(jobPosition.getContract_type_id());
        Company company = GeneralController.getInstance().getCompanyController()
                .searchById(jobPosition.getCompany_id());
        return String.format("title : %s, description : %s, view count : %s, company : %s, type : %s",
                jobPosition.getTitle(), jobPosition.getDescription(), jobPosition.getView_count(),
                company.getName(), contractType.getType());
    }

    public void handleUpdate(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/employee_update.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }
}
