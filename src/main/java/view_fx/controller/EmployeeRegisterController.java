package view_fx.controller;

import controller.GeneralController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import model.Company;
import model.Employee;
import model.Job;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

public class EmployeeRegisterController {

    @FXML
    private AnchorPane root;

    @FXML
    private TextField name;

    @FXML
    private TextField username;

    @FXML
    private TextField password;

    @FXML
    private TextField age;

    @FXML
    private TextField email;

    @FXML
    private TextField telephone;

    @FXML
    private TextField work_experience;

    @FXML
    private CheckBox vip;

    @FXML
    void handleBack(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/register.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void handleRegister(MouseEvent event) throws IOException {

        List<Job> jobs = GeneralController.getInstance().getJobController().findAll();
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/employee_register_job.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        FlowPane flowPane = new FlowPane(30 , 10);

        for (int i = 0; i < jobs.size(); i++) {
            Job job = jobs.get(i);
            Button button = new Button(String.format("job title : %s", job.getTitle()));
            flowPane.getChildren().add(button);
            button.setOnMouseClicked(e -> {

                Employee employee = new Employee(name.getText(), username.getText(), password.getText(), Integer.parseInt(age.getText()),
                        job.getId(), email.getText(), telephone.getText(), work_experience.getText(), vip.isSelected());
                GeneralController.getInstance().getEmployeeController().insert(employee);

                try {
                    URL url2 = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                            "src/main/java/view_fx/fxml/login.fxml").toURI().toURL();
                    AnchorPane anchorPane2 = FXMLLoader.load(url2);
                    button.getScene().setRoot(anchorPane2);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
        }

        anchorPane.getChildren().add(flowPane);
        this.root.getScene().setRoot(anchorPane);
    }
}
