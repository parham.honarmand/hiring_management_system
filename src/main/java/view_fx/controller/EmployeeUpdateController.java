package view_fx.controller;

import controller.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import model.*;
import utility.Encryptor;

import java.io.*;
import java.net.URL;
import java.util.List;

public class EmployeeUpdateController {

    @FXML
    private AnchorPane root;

    @FXML
    private TextField name;

    @FXML
    private TextField username;

    @FXML
    private TextField password;

    @FXML
    private TextField age;

    @FXML
    private TextField email;

    @FXML
    private TextField telephone;

    @FXML
    private TextField work_experience;

    @FXML
    private CheckBox vip;

    private boolean isSet;

    @FXML
    void handleBack(MouseEvent event) throws IOException {
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/employee_panel.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    public void updateEmployee(MouseEvent event) throws IOException {
        Employee current = EmployeeController.currentEmployee;
        String pass = (password.getText().equals("")) ? current.getPassword() : Encryptor.getMd5(password.getText());

        List<Job> jobs = GeneralController.getInstance().getJobController().findAll();
        URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                "src/main/java/view_fx/fxml/employee_update_job.fxml").toURI().toURL();
        AnchorPane anchorPane = FXMLLoader.load(url);
        FlowPane flowPane = new FlowPane(30, 10);

        for (int i = 0; i < jobs.size(); i++) {
            Job job = jobs.get(i);
            Button button = new Button(String.format("job title : %s", job.getTitle()));
            flowPane.getChildren().add(button);
            button.setOnMouseClicked(e -> {
                Employee employee = new Employee(current.getId(), name.getText(), username.getText(), pass,
                        Integer.parseInt(age.getText()), job.getId(), email.getText(), telephone.getText(), work_experience.getText(),
                        vip.isSelected());
                GeneralController.getInstance().getEmployeeController().update(employee);
                EmployeeController.currentEmployee = employee;

                try {
                    URL url2 = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                            "src/main/java/view_fx/fxml/employee_panel.fxml").toURI().toURL();
                    AnchorPane anchorPane2 = FXMLLoader.load(url2);
                    button.getScene().setRoot(anchorPane2);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
        }
        isSet = false;
        anchorPane.getChildren().add(flowPane);
        this.root.getScene().setRoot(anchorPane);
    }

    @FXML
    void setValue() {
        if (isSet)
            return;
        Employee employee = EmployeeController.currentEmployee;

        name.setText(employee.getName());
        username.setText(employee.getUsername());
        age.setText(String.valueOf(employee.getAge()));
        email.setText(employee.getEmail());
        telephone.setText(employee.getTelephone());
        work_experience.setText(employee.getWork_experience());
        vip.setSelected(employee.is_vip());
        isSet = true;
    }
}
