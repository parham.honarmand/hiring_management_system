package view_fx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.*;
import java.net.*;

public class Main extends Application {
    AnchorPane root;

    //use this for vm option : --module-path /home/parham/Desktop/new/libs/javafx-sdk-11.0.2/lib --add-modules javafx.controls,javafx.fxml
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() throws Exception {
        try {
            URL url = new File("/home/parham/Desktop/new/Mapsa projects/hiring_management_system/" +
                    "src/main/java/view_fx/fxml/main.fxml").toURI().toURL();
            root = (AnchorPane) FXMLLoader.load(url);

        } catch (MalformedURLException e) {
            throw new RuntimeException("Exception in URL");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception while loading fxmls");
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
            Scene scene = new Scene(root);
            stage.setTitle("Welcome");
            stage.setScene(scene);
            stage.show();
    }
}
