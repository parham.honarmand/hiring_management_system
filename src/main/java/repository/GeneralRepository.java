package repository;

import repository.JDBCImpl.*;

public class GeneralRepository {
    public static GeneralRepository generalRepository;
    private RepositoriesType repositoriesType = RepositoriesType.JDBC;
    private CompanyDao companyDao;
    private EmployeeDao employeeDao;
    private JobPositionDao jobPositionDao;
    private JobDao jobDao;
    private ContractTypeDao contractTypeDao;
    private ApplyDao applyDao;

    public GeneralRepository() {
        switch (repositoriesType) {
            case JDBC:
                companyDao = new CompanyDao();
                employeeDao = new EmployeeDao();
                jobPositionDao = new JobPositionDao();
                jobDao = new JobDao();
                contractTypeDao = new ContractTypeDao();
                applyDao = new ApplyDao();
                break;
            case HIBERNATE:
                break;
        }
    }

    public static GeneralRepository getInstance() {
        if (generalRepository == null)
            generalRepository = new GeneralRepository();
        return generalRepository;
    }

    public CompanyDao getCompanyDao() {
        return companyDao;
    }

    public EmployeeDao getEmployeeDao() {
        return employeeDao;
    }

    public JobPositionDao getJobPositionDao() {
        return jobPositionDao;
    }

    public JobDao getJobDao() {
        return jobDao;
    }

    public ContractTypeDao getContractTypeDao() {
        return contractTypeDao;
    }

    public ApplyDao getApplyDao() {
        return applyDao;
    }
}
