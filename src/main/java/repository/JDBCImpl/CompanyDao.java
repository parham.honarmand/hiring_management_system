package repository.JDBCImpl;

import connectionManager.*;
import model.Company;
import repository.CompanyRepository;
import utility.ObjectNotFoundException;

import java.sql.*;
import java.util.*;

public class CompanyDao implements CompanyRepository {
    public ConnectionManager connectionManager = new MySqlConnectionManager();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into company (name, username, password, location, field, description," +
            " employee_count) values (?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_BY_ID_QUERY = "select * from company where company.id = ?";
    private static final String SELECT_ALL_QUERY = "select * from company";
    private static final String UPDATE_QUERY = "update company set name = ?, username = ?, password = ?, " +
            "location = ?, field = ?, description = ?, employee_count = ? where company.id = ?";
    private static final String DELETE_QUERY = "delete from company where company.id = ?";
    private static final String SELECT_BY_USERNAME_QUERY = "select * from company where company.username = ?";

    public void insert(Company company) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setString(1, company.getName());
        statement.setString(2, company.getUsername());
        statement.setString(3, company.getPassword());
        statement.setString(4, company.getLocation());
        statement.setString(5, company.getField());
        statement.setString(6, company.getDescription());
        statement.setInt(7, company.getEmployee_count());
        statement.executeUpdate();
    }

    public Company searchById(Long id) throws ObjectNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Company(resultSet.getLong(1),
                    resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
                    resultSet.getString(5), resultSet.getString(6), resultSet.getString(7),
                    resultSet.getInt(8));

        throw new ObjectNotFoundException("this company does not exist");
    }

    public List<Company> findAll() throws SQLException {
        List<Company> companies = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            companies.add(new Company(resultSet.getLong(1),
                    resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
                    resultSet.getString(5), resultSet.getString(6), resultSet.getString(7),
                    resultSet.getInt(8)));

        return companies;
    }

    public void update(Company company) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setString(1, company.getName());
        statement.setString(2, company.getUsername());
        statement.setString(3, company.getPassword());
        statement.setString(4, company.getLocation());
        statement.setString(5, company.getField());
        statement.setString(6, company.getDescription());
        statement.setInt(7, company.getEmployee_count());
        statement.setLong(8, company.getId());
        statement.executeUpdate();
    }

    public void delete(Long id) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    public Company searchByUsername(String username) throws SQLException, ObjectNotFoundException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_USERNAME_QUERY);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Company(resultSet.getLong(1),
                    resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
                    resultSet.getString(5), resultSet.getString(6), resultSet.getString(7),
                    resultSet.getInt(8));

        throw new ObjectNotFoundException("this company does not exist");
    }
}
