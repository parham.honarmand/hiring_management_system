package repository.JDBCImpl;

import connectionManager.*;
import model.Job;
import repository.JobRepository;
import utility.ObjectNotFoundException;

import java.sql.*;
import java.util.*;

public class JobDao implements JobRepository {
    public ConnectionManager connectionManager = new MySqlConnectionManager();
    private PreparedStatement statement;
    private static final String INSERT_QUERY = "insert into job (title) values (?)";
    private static final String SELECT_BY_ID_QUERY = "select * from job where job.id = ?";
    private static final String SELECT_ALL_QUERY = "select * from job";
    private static final String UPDATE_QUERY = "update job set title = ? where job.id = ?";
    private static final String DELETE_QUERY = "delete from job where job.id = ?";

    public void insert(Job job) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(INSERT_QUERY);
        statement.setString(1, job.getTitle());
        statement.executeUpdate();
    }

    public Job searchById(Long id) throws ObjectNotFoundException, SQLException {
        statement = connectionManager.getConnection().prepareStatement(SELECT_BY_ID_QUERY);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            return new Job(resultSet.getLong(1), resultSet.getString(2));

        throw new ObjectNotFoundException("this job does not exist");
    }

    public List<Job> findAll() throws SQLException {
        List<Job> contractTypes = new ArrayList<>();
        statement = connectionManager.getConnection().prepareStatement(SELECT_ALL_QUERY);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next())
            contractTypes.add(new Job(resultSet.getLong(1), resultSet.getString(2)));

        return contractTypes;
    }

    public void update(Job contractType) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(UPDATE_QUERY);
        statement.setString(1, contractType.getTitle());
        statement.setLong(2, contractType.getId());
        statement.executeUpdate();
    }

    public void delete(Long id) throws SQLException {
        statement = connectionManager.getConnection().prepareStatement(DELETE_QUERY);
        statement.setLong(1, id);
        statement.executeUpdate();
    }
}
