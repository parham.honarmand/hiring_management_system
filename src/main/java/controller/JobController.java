package controller;

import model.Job;
import service.GeneralService;
import service.JobService;
import utility.ObjectNotFoundException;

import java.util.List;

public class JobController implements Controller<Job> {
    private JobService jobService = GeneralService.getInstance().getJobService();

    public void insert(Job job) {
        jobService.insert(job);
    }

    public void update(Job job) {
        jobService.update(job);
    }

    public Job searchById(Long id) throws ObjectNotFoundException {
        return jobService.searchById(id);
    }

    public List<Job> findAll() {
        return jobService.findAll();
    }
}
