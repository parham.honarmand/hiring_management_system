package controller;

import utility.ObjectNotFoundException;

import java.util.List;

public interface Controller<T> {
    void insert(T t);

    void update(T t);

    T searchById(Long id) throws ObjectNotFoundException;

    List<T> findAll();
}
