package controller;

import model.ContractType;
import service.ContractTypeService;
import service.GeneralService;
import utility.ObjectNotFoundException;

import java.util.List;

public class ContractTypeController implements Controller<ContractType> {
    private ContractTypeService contractTypeService = GeneralService.getInstance().getContractTypeService();

    public void insert(ContractType contractType) {
        contractTypeService.insert(contractType);
    }

    public void update(ContractType contractType) {
        contractTypeService.update(contractType);
    }

    public ContractType searchById(Long id) throws ObjectNotFoundException {
        return contractTypeService.searchById(id);
    }

    public List<ContractType> findAll() {
        return contractTypeService.findAll();
    }
}
