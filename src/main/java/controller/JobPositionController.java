package controller;

import model.JobPosition;
import service.GeneralService;
import service.JobPositionService;
import utility.ObjectNotFoundException;

import java.util.List;

public class JobPositionController implements Controller<JobPosition> {
    private JobPositionService jobPositionService = GeneralService.getInstance().getJobPositionService();

    public void insert(JobPosition jobPosition) {
        jobPositionService.insert(jobPosition);
    }

    public void update(JobPosition jobPosition) {
        jobPositionService.update(jobPosition);
    }

    public JobPosition searchById(Long id) throws ObjectNotFoundException {
        return jobPositionService.searchById(id);
    }

    public List<JobPosition> findAll() {
        return jobPositionService.findAll();
    }

    public List<JobPosition> searchByCompanyId(Long id) {
        return jobPositionService.searchByCompanyId(id);
    }

    public void updateByViewCount(JobPosition jobPosition) {
        jobPositionService.updateByViewCount(jobPosition);
    }
}
