package controller;

import model.Company;
import service.CompanyService;
import service.GeneralService;
import utility.ObjectNotFoundException;

import java.util.List;

public class CompanyController implements Controller<Company> {
    public static Company currentCompany;
    private CompanyService companyService = GeneralService.getInstance().getCompanyService();

    public void insert(Company company) {
        companyService.insert(company);
    }

    public void update(Company company) {
        companyService.update(company);
    }

    public Company searchById(Long id) throws ObjectNotFoundException {
        return companyService.searchById(id);
    }

    public List<Company> findAll() {
        return companyService.findAll();
    }

    public Company searchByUsername(String username) {
        return companyService.searchByUsername(username);
    }

    public Company login(String username, String password) {
        return companyService.login(username, password);
    }
}
